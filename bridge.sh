#!/bin/bash

set -e

declare -a IFACES
BRIDGE="mitm0"
NETWORKMANAGER="y"
WAIT="y"

usage() {
    echo "usage: $(basename "$0") [-b BIFACE] -i IFACE [-i IFACE]... [-mnh]"
    echo ''
    echo 'This script sets up a network bridge called "mitm0".'
    echo 'Network interfaces specified with -i are connected to the bridge.'
    echo 'The script stays open waiting for a SIGINT by issueing CTRL-C'
    echo 'in order to perform proper cleanup.'
    echo ''
    echo 'options:'
    echo ' -b   Use a different bridge device name'
    echo ' -m   Do not notify NetworkManager'
    echo ' -n   NoWait; do not stay open'
    echo ' -h   Show this page and exit'
}

cleanup() {
    for iface in "${IFACES[@]}"; do
        if [[ "$NETWORKMANAGER" == "y" ]]; then
            nmcli dev set "$iface" managed yes
        fi
    done

    ip link del "$BRIDGE"
}

main() {
    if [[ "$WAIT" == "y" ]]; then
        trap 'cleanup' SIGINT SIGTERM EXIT
    else
        trap 'cleanup' EXIT
    fi

    ip link add "$BRIDGE" type bridge
    ip link set "$BRIDGE" up

    for iface in "${IFACES[@]}"; do
        ip link set "$iface" master "$BRIDGE"
        ip link set "$iface" up

        if [[ "$NETWORKMANAGER" == "y" ]]; then
            nmcli dev set "$iface" managed no
        fi
    done

    # Block and wait for CTRL+C.
    if [[ "$WAIT" == "y" ]]; then
        sleep infinity
    fi
}

while getopts "b:i:mnh" arg; do
    case "$arg" in
        b) BRIDGE="$OPTARG";;
        i) IFACES+=("$OPTARG");;
        n) WAIT="n";;
        m) NETWORKMANAGER="n";;
        h) usage && exit 0;;
        *) usage && exit 1;;
    esac
done

if (( ${#IFACES[@]} == 0)); then
    echo 'please set at least one interface with -i!'
    exit 1
fi

main
