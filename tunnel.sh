#!/bin/bash

set -eu

# $1: which peer: A or B
# $2: local ip address
# $3: remote ip address
# $4: mtu
l2tpsetup() {
    local tunnel_id
    local peer_tunnel_id
    local session_id
    local peer_session_id
    local local_addr
    local remote_addr
    local udp_sport
    local udp_dport
    local mtu

    local_addr="$2"
    remote_addr="$3"
    mtu="$4"

    # These come from the manpage. :-)
    if [[ "$1" == "A" ]]; then
        tunnel_id="3000"
        peer_tunnel_id="4000"
        session_id="1000"
        peer_session_id="2000"
        udp_sport="5000"
        udp_dport="6000"
    else
        tunnel_id="4000"
        peer_tunnel_id="3000"
        session_id="2000"
        peer_session_id="1000"
        udp_sport="6000"
        udp_dport="5000"
    fi

    ip l2tp add tunnel \
        tunnel_id "$tunnel_id" \
        peer_tunnel_id "$peer_tunnel_id" \
        encap udp \
        local "$local_addr" \
        remote "$remote_addr" \
        udp_sport "$udp_sport" \
        udp_dport "$udp_dport"

    ip l2tp add session \
        tunnel_id "$tunnel_id" \
        session_id "$session_id" \
        peer_session_id "$peer_session_id"

    ip link set l2tpeth0 up mtu "$mtu"
}

# TODO
brsetup() {
    ip link set l2tpeth0 up mtu 1446
    ip link add br0 type bridge
    ip link set l2tpeth0 master br0
    ip link set eth0 master br0
    ip link set br0 up
}


