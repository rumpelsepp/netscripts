# netscripts

This is aimed to be a collection of shell scripts that are intended
to make your life as a networking guy easier. There are common annoying
tasks which just take time but need to be done sometimes, for instance
setting up an ad-hoc DHCP server which announces custom DNS servers,
or building a network bridge to fire up wireshark…

Stuff like this is collected and maintained in this repository.
