#!/bin/bash

set -e

DNS="internal"
GATEWAY="192.168.100.1/24"

NAT_IFACE=""
DHCP_IFACE=""
DHCP_RANGE="192.168.100.50,192.168.100.100,12h"
FIREWALL="nft"
NETWORKMANAGER="y"
SYSTEMD="n"
SKIPNAT="n"
LOGQUERIES="n"
VERBOSE="n"
ETHERS="n"

get_iface() {
    local query=".[] | select(.ifindex == $1 and .link_type == \"ether\") | .ifname"
    ip -j link show | jq -r "$query"
}

nft_enable_nat() {
    nft "add table nat"
    nft "add chain nat prerouting { type nat hook prerouting priority 0; }"
    nft "add chain nat postrouting { type nat hook postrouting priority 0; }"
    nft "add rule nat postrouting oif $NAT_IFACE masquerade"
}

nft_flush_ruleset() {
    nft "delete table nat"
}

enable_nat() {
    case "$FIREWALL" in
        nft)      nft_enable_nat;;
        *)        echo "invalid firewall" && exit 1;;
    esac
}

flush_ruleset() {
    case "$FIREWALL" in
        nft)      nft_flush_ruleset;;
        *)        echo "invalid firewall" && exit 1;;
    esac
}

cleanup() {
    echo ""
    echo "Cleaning up. Disabling routing and nat."

    if [[ "$SKIPNAT" == "n" ]]; then
        flush_ruleset
        sysctl net.ipv4.ip_forward=0
    fi

    ip addr del $GATEWAY dev $DHCP_IFACE
    ip link set dev $DHCP_IFACE down

    if [[ $NETWORKMANAGER == 'y' ]]; then
        nmcli dev set "$DHCP_IFACE" managed yes
    fi

    if [[ $SYSTEMD == 'y' ]]; then
        systemctl stop mitm-router.service
    fi
}

main() {
    trap 'cleanup' SIGINT SIGTERM

    if [[ "$SKIPNAT" == "n"  ]]; then
        # Enable router
        sysctl net.ipv4.ip_forward=1
        enable_nat
    fi

    # Avoid that networkmanager re-configures this interface.
    if [[ $NETWORKMANAGER == 'y' ]]; then
        nmcli dev set "$DHCP_IFACE" managed no
    fi

    # Configure ethernet card
    ip addr add $GATEWAY dev $DHCP_IFACE
    ip link set dev $DHCP_IFACE up

    local opts
    opts=("--interface=$DHCP_IFACE"
          "--except-interface=lo"
          "--bind-interfaces"
          "--dhcp-authoritative"
          "--dhcp-range=$DHCP_RANGE")

    if [[ "$DNS" != "internal" ]]; then
        local dhcp_opts
        dhcp_opts="6,${DNS}"  # RFC 2132, DNS option

        opts+=("--dhcp-option=$dhcp_opts")
    fi

    if [[ "$ETHERS" == "y" ]]; then
        opts+=("--read-ethers")
    fi

    if [[ "$LOGQUERIES" == "y" ]]; then
        opts+=("--log-queries")
    fi

    if [[ "$VERBOSE" == "y" ]]; then
        opts+=("--log-dhcp")
    fi

    echo "dnsmasq options: $(echo "${opts[@]}" | tr -d '\t')"

    if [[ "$SYSTEMD" == "y" ]]; then
        opts+=("--keep-in-foreground")

        systemd-run --unit mitm-router dnsmasq "${opts[@]}"
        journalctl -f -u mitm-router.service
    else
        opts+=("--no-daemon")

        dnsmasq "${opts[@]}"
    fi
}

usage() {
    echo "usage: $(basename "$0") [-d SERVER] [-i IFACE] [-o IFACE]"
    echo '       [-g IP] [-r RANGE] [-psnlvh]'
    echo ''
    echo 'This script spawns dnsmasq as a DHCP and DNS server.'
    echo 'Routing and NAT is set up automatically.'
    echo ''
    echo 'If no interface are specified, the script tries autodetection'
    echo 'where the second available (sorted by index) ethernet device'
    echo 'will be configured to run the DHCP. Traffic will be routed'
    echo 'through the first ethernet interface.'
    echo ''
    echo 'options:'
    echo ' -d     Announce a different DNS server'
    echo ' -i     Run DHCP on this interface'
    echo ' -o     Route traffic through this interface'
    echo ' -n     Ignore NetworkManager'
    echo ' -l     Log DNS queries (only works if -d is not set)'
    echo ' -s     Run dnsmasq with systemd-run; logging to journal'
    echo ' -g     Assign this IP address to the DHCP interface. See also -r.'
    echo ' -r     DHCP range; e.g. 192.168.100.50,192.168.100.100,12h'
    echo ' -y     dnsmasq only, skip NAT setup'
    echo ' -z     Read /etc/ethers'
    echo ' -v     Be verbose, more logging'
    echo ' -h     Show this page and exit'
}

while getopts "d:i:o:r:g:psnlyzvh" arg; do
    case "$arg" in
        d)  DNS="$OPTARG";;
        i)  DHCP_IFACE="$OPTARG";;
        o)  NAT_IFACE="$OPTARG";;
        g)  GATEWAY="$OPTARG";;
        n)  NETWORKMANAGER="n";;
        s)  SYSTEMD="y";;
        r)  DHCP_RANGE="$OPTARG";;
        l)  LOGQUERIES="y";;
        y)  SKIPNAT="y";;
        z)  ETHERS="y";;
        v)  VERBOSE="y";;
        h)  usage && exit 0;;
        *)  usage && exit 1;;
    esac
done

# For autodetection
# Indexes start at 1 where 1 is usually the loopback interface.
natiface_index=2
dhcpiface_index=3

if [[ "$SKIPNAT" == "y" ]]; then
    dhcpiface_index=2
fi

if [[ -z "$NAT_IFACE" && "$SKIPNAT" == "n" ]]; then
    NAT_IFACE="$(get_iface $natiface_index)"
    if [[ -z "$NAT_IFACE" ]]; then
        echo "auto detection for \$NAT_IFACE failed"
        exit 1
    fi

    echo "auto detected \$NAT_IFACE : $NAT_IFACE"
fi

if [[ -z "$DHCP_IFACE" ]]; then
    DHCP_IFACE="$(get_iface $dhcpiface_index)"
    if [[ -z "$DHCP_IFACE" ]]; then
        echo "auto detection for \$DHCP_IFACE failed"
        exit 1
    fi

    echo "auto detected \$DHCP_IFACE: $DHCP_IFACE"
fi

main
